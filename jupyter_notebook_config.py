import os

c.SparkConnectorApp.disable_cluster_selection_on_preselected = True
c.SparkConnectorApp.error_suggestions = [
    {
        "pattern": "org.apache.hadoop.security.AccessControlException: Client cannot authenticate via:[TOKEN, KERBEROS]",
        "type": "info",
        "message": "Please authenticate using <code>kinit</code> first.",
    },
]

spark_ports_env = os.getenv('SPARK_PORTS', '')
driver_port, block_manager_port, ui_port = spark_ports_env.split(',')[:3]

c.SparkConnectorApp.clusters = {
    "k8s": {
        "display_name": "Cloud Containers (k8s)",
        "env": {},
        "pre_script": ". /srv/singleuser/sparkconfig.sh",
    },
    "analytix": {
        "display_name": "Analytix",
        "env": {},
        "pre_script": ". /srv/singleuser/sparkconfig.sh",
        "webui_hostname": os.getenv('SERVER_HOSTNAME', 'localhost'),
        "webui_port": int(ui_port),
        "opts": {
            "spark.driver.host": os.getenv('SERVER_HOSTNAME', 'localhost'),
            "spark.driver.port": driver_port,
            "spark.driver.blockManager.port": block_manager_port,
            "spark.port.maxRetries": '100',
            "spark.driver.extraJavaOptions": "-Djavax.net.ssl.trustStore=/etc/ssl/certs/truststore.jks -Djavax.net.ssl.trustStorePassword=password"
        },
        "spark_webui_proxy_host_allowlist": [
            "localhost",
            os.getenv('SERVER_HOSTNAME', 'localhost'),
            "ithdp6014.cern.ch",
            "ithdp6013.cern.ch",
            "ithdp6014.cern.ch:8088",
            "ithdp6013.cern.ch:8088",
        ]
    },
}

c.SparkConnectorApp.config_bundles_from_file = {
    "file": "/cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/bundles/bundles.json",
    "json_path": "bundled_options",
}

c.SparkConnectorApp.config_bundles = {}

c.SparkConnectorApp.spark_options_from_file = {
    "file": "/cvmfs/sft.cern.ch/lcg/etc/hadoop-confext/bundles/spark_options.json",
    "json_path": "spark_options",
}

c.SparkConnectorApp.spark_options = [
    {
        "data": {
            "category": "Yarn AmIpFilter"
        }, 
        "value": "spark.org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter.param.PROXY_URI_BASES"
    },
    {
        "data": {
            "category": "Yarn AmIpFilter"
        }, 
        "value": "spark.org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter.param.PROXY_URI_BASE"
    },
    {
        "data": {
            "category": "Yarn AmIpFilter"
        }, 
        "value": "spark.org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter.param.PROXY_HOSTS"
    },
    {
        "data": {
            "category": "Yarn AmIpFilter"
        }, 
        "value": "spark.org.apache.hadoop.yarn.server.webproxy.amfilter.AmIpFilter.param.PROXY_HOST"
    },
]

c.RucioConfig.instances = [
    {
        "name": "experiment.cern.ch",
        "display_name": "Experiment",
        "rucio_base_url": "https://rucio",
        "rucio_auth_url": "https://rucio",
        "rucio_ca_cert": "/path/to/rucio_ca.pem",
        "destination_rse": "SWAN-EOS",
        "rse_mount_path": "/eos/rucio",
        "path_begins_at": 4,
        "mode": "replica",
    }
]
