#!/bin/bash

# Setup LCG
export LCG_VIEW=$ROOT_LCG_VIEW_PATH/$ROOT_LCG_VIEW_NAME/$ROOT_LCG_VIEW_PLATFORM
export SPARK_JAVA_OPTS="$SPARK_JAVA_OPTS -Djavax.net.ssl.trustStore=/etc/ssl/certs/truststore.jks -Djavax.net.ssl.trustStorePassword=password"
source $LCG_VIEW/setup.sh

log_info() {
    echo "[INFO $(date '+%Y-%m-%d %T.%3N') $(basename $0)] $1"
}

# Configure SparkConnector
if [[ $SPARK_CLUSTER_NAME ]]; 
then
 START_TIME_SETUP_SPARK=$( date +%s.%N )

 log_info "Configuring environment for Spark cluster: $SPARK_CLUSTER_NAME"
 # detect Spark major version to choose different Spark configuration 
 # the second argument of $SPARK_CONFIG_SCRIPT is the classpath compatibility for yarn
 SPARK_MJR_VERSION=$(readlink -f `which pyspark` | awk -F\/ '{print substr($7,1,1)}')
 HADOOP_MJR_VERSION=$(readlink -f `which hdfs` | awk -F\/ '{print substr($7,1,1)}')
 if [[ $SPARK_MJR_VERSION == 3 ]]; then SPARKVERSION=spark3; fi
 if [[ $HADOOP_MJR_VERSION == 3 ]]; then HADOOPVERSION='3.2'; fi
 source $SPARK_CONFIG_SCRIPT $SPARK_CLUSTER_NAME ${HADOOPVERSION:-'2.7'} ${SPARKVERSION:-'spark2'}
 #to make sure we get the ipv4 addrress when in dual stack nodes
 export SPARK_LOCAL_IP=$(getent ahostsv4 | awk "/$HOSTNAME/ {print \$1}")
 log_info "Completed Spark Configuration"

 SETUP_SPARK_TIME_SEC=$(echo $(date +%s.%N --date="$START_TIME_SETUP_SPARK seconds ago") | bc)
 log_info "user: $USER, host: ${SERVER_HOSTNAME%%.*}, metric: configure_user_env_spark.${ROOT_LCG_VIEW_NAME:-none}.${SPARK_CLUSTER_NAME:-none}.duration_sec, value: $SETUP_SPARK_TIME_SEC"
fi
